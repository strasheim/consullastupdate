// Checking a timestamp in the consul KV. Timestaamp need to be unix epoch
package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/hashicorp/consul/api"
	"github.com/quipo/statsd"
)

func main() {
	if len(os.Args) != 3 {
		check("usage:" + os.Args[0] + " <kv_to_watch> <number_of_seconds>")
	}

	pastSeconds, err := strconv.ParseInt(os.Args[2], 10, 64)
	check(err)

	client, err := api.NewClient(api.DefaultConfig())
	check(err)

	kv := client.KV()
	pair, _, err := kv.Get(os.Args[1], nil)
	check(err)
	if pair == nil {
		check("No data was found for the given key: " + os.Args[1])
	}
	lastUpdate, err := strconv.ParseInt((string(pair.Value)), 10, 64)
	check(err)

	prefix := strings.Replace(strings.Replace(os.Args[1], "lastupdated", "", -1), "/", ".", -1)
	statspush := statsd.NewStatsdClient("127.0.0.1:8125", prefix)
	err = statspush.CreateSocket()
	check(err)
	defer statspush.Close()

	statspush.Gauge("lastUpdated", lastUpdate)

	rightnow := time.Now()
	someTimeAgo := rightnow.Unix() - pastSeconds

	if lastUpdate < someTimeAgo {
		check("Last Update for" + fmt.Sprint(pair.Key) + "is too old. [" + fmt.Sprint(lastUpdate) + "]")
	}
	fmt.Println(pair.Key, "for", pair.Key, "was updated in time")
	os.Exit(0)
}

func check(err interface{}) {
	if err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}
