## Last Update

A very small tool to take 1 or 2 arguments, the KV value which should be a timestamp to read and compare against 1 minute ago or against the number of seconds set in the 2nd arguments. If it's older report failure and exit with 2 otherwise 0. 

Output is geared for consul consumption. It's does not change strings all the time, to hide away dublicated output from the propagation layer. 

### Usage

Usage on the commandline 

```
consullastupdate /path/to/timestamp 120
```

#### Stats	
Consullastupdate emits statsD information. It will push the timestamp to statsD if found. 

